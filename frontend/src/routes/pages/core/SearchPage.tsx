import { FC } from 'react'
import SearchForm from '../../../components/core/SearchForm'

const SearchPage: FC = () => {
    return <SearchForm />
}

export default SearchPage
