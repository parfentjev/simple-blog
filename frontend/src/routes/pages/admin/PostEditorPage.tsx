import { FC } from 'react'

import PostEditor from '../../../components/admin/editor/PostEditor'

const PostEditorPage: FC = () => {
    return <PostEditor />
}

export default PostEditorPage
