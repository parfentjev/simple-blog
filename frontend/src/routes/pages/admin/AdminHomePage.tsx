import { FC } from 'react'
import AdminHome from '../../../components/admin/AdminHome'

const AdminHomePage: FC = () => {
    return <AdminHome />
}

export default AdminHomePage
