import { FC } from 'react'
import AuthForm from '../../../components/admin/AuthForm'

const AuthPage: FC = () => {
    return <AuthForm />
}

export default AuthPage
