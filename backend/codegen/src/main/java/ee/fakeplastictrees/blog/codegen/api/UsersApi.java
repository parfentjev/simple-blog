/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (7.10.0-SNAPSHOT).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package ee.fakeplastictrees.blog.codegen.api;

import ee.fakeplastictrees.blog.codegen.model.TokenDto;
import ee.fakeplastictrees.blog.codegen.model.UsersPostRequest;
import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import jakarta.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", comments = "Generator version: 7.10.0-SNAPSHOT")
@Validated
@Tag(name = "Users", description = "the Users API")
public interface UsersApi {

    /**
     * POST /users
     * Create a new user
     *
     * @param usersPostRequest  (required)
     * @return User created (status code 201)
     *         or Bad request body (status code 400)
     *         or Registration is disabled (status code 403)
     *         or Internal server error (status code 500)
     */
    @Operation(
        operationId = "usersPost",
        description = "Create a new user",
        tags = { "Users" },
        responses = {
            @ApiResponse(responseCode = "201", description = "User created", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = TokenDto.class))
            }),
            @ApiResponse(responseCode = "400", description = "Bad request body"),
            @ApiResponse(responseCode = "403", description = "Registration is disabled"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
        }
    )
    @RequestMapping(
        method = RequestMethod.POST,
        value = "/users",
        produces = { "application/json" },
        consumes = { "application/json" }
    )
    
    ResponseEntity<TokenDto> usersPost(
        @Parameter(name = "UsersPostRequest", description = "", required = true) @Valid @RequestBody UsersPostRequest usersPostRequest
    );


    /**
     * POST /users/token
     * Generate a user token
     *
     * @param usersPostRequest  (required)
     * @return Token created (status code 200)
     *         or Bad request body (status code 400)
     *         or Internal server error (status code 500)
     */
    @Operation(
        operationId = "usersTokenPost",
        description = "Generate a user token",
        tags = { "Users" },
        responses = {
            @ApiResponse(responseCode = "200", description = "Token created", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = TokenDto.class))
            }),
            @ApiResponse(responseCode = "400", description = "Bad request body"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
        }
    )
    @RequestMapping(
        method = RequestMethod.POST,
        value = "/users/token",
        produces = { "application/json" },
        consumes = { "application/json" }
    )
    
    ResponseEntity<TokenDto> usersTokenPost(
        @Parameter(name = "UsersPostRequest", description = "", required = true) @Valid @RequestBody UsersPostRequest usersPostRequest
    );

}
